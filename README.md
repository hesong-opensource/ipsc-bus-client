# ipsc-bus-client-c

`IPSC` 服务程序数据总线客户端的 `C` 库。

通过这个库，应用程序可以“客户端”的形式与 `IPSC` 服务器互控。
