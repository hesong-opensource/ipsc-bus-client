
#if !defined(_SMARTBUS_NETCLI_INTERFACE_H_INCLUDED_)
#define _SMARTBUS_NETCLI_INTERFACE_H_INCLUDED_

#include "smartbus.h"

#ifdef WIN32
#ifdef SMARTBUS_NET_CLI_EXPORTS
#define SMARTBUS_NET_CLI_API __declspec(dllexport)
#else
#define SMARTBUS_NET_CLI_API __declspec(dllimport)
#endif
#else
#define SMARTBUS_NET_CLI_API
#endif

#ifdef __cplusplus
extern "C" {
#endif

///////////////////////////////////////////////////////////////////
#ifdef WIN32
#ifndef CDECL
#define CDECL   __cdecl
#endif
#else
#define CDECL
#endif


/**
 * @brief      设置Trace函数
 *
 * @param[in]  traceex   The traceex
 * @param[in]  traceerr  The traceerr
 */
SMARTBUS_NET_CLI_API void CDECL SmartBusNetCli_SetTrace(PTraceEx traceex, PTraceEx traceerr);


/**
 * @brief      设置Trace函数
 *
 * @param[in]  traceex   The traceex
 * @param[in]  traceerr  The traceerr
 */
SMARTBUS_NET_CLI_API void CDECL SmartBusNetCli_SetTraceStr(PTraceStr traceex, PTraceStr traceerr);


/**
 * @brief      初始化
 *
 * @param[in]  unitid  客户端ID。unitid >= 16
 *
 * @return     { description_of_the_return_value }
 */
SMARTBUS_NET_CLI_API int CDECL SmartBusNetCli_Init(unsigned char unitid);


/**
 * @brief      初始化
 *
 * @param[in]  unitid  客户端ID。unitid >= 16
 * @param[in]  option  The option
 *
 * @return     { description_of_the_return_value }
 */
SMARTBUS_NET_CLI_API int CDECL SmartBusNetCli_InitEx(unsigned char unitid, const char * option);


// 释放
SMARTBUS_NET_CLI_API void CDECL SmartBusNetCli_Release();


/**
 * @brief      设置回调函数
 *
 * @param[in]  client_conn_cb     客户端连接结果
 * @param[in]  recv_cb            接收数据回调
 * @param[in]  disconnect_cb      连接端口回调
 * @param[in]  invokeflow_ret_cb  调用流程返回回掉
 * @param[in]  global_connect_cb  全局连接回掉
 * @param      arg                The argument
 */
SMARTBUS_NET_CLI_API void CDECL SmartBusNetCli_SetCallBackFn(smartbus_cli_connection_cb client_conn_cb,
        smartbus_cli_recvdata_cb recv_cb, smartbus_cli_disconnect_cb disconnect_cb,
        smartbus_invokeflow_ret_cb invokeflow_ret_cb, smartbus_global_connect_cb global_connect_cb,
        void * arg);


/**
 * @brief      设置回调函数自定义数据
 *
 * @param      arg   自定义数据
 */
SMARTBUS_NET_CLI_API void CDECL  SmartBusNetCli_SetCallBackFnArg(void * arg);


/**
 * @brief      设置回调函数
 *
 * @param[in]  callback_name  待设定的回调函数名称
 * @param      callbackfn     回调函数指针
 */
SMARTBUS_NET_CLI_API void CDECL  SmartBusNetCli_SetCallBackFnEx(const char * callback_name, void * callbackfn);


/**
 * @brief      创建连接
 *
 * @param[in]  local_clientid    本地clientid, >= 0 and <= 255
 * @param[in]  local_clienttype  本地clienttype
 * @param[in]  masterip          目标主IP地址
 * @param[in]  masterport        目标主端口
 * @param[in]  slaverip          目标从IP地址。没有从地址的，填写0，或者""
 * @param[in]  slaverport        目标从端口。没有从端口的，填写0xFFFF
 * @param[in]  author_username   验证用户名
 * @param[in]  author_pwd        验证密码
 * @param[in]  add_info          附加信息
 *
 * @return     { description_of_the_return_value }
 */
SMARTBUS_NET_CLI_API int CDECL SmartBusNetCli_CreateConnect(unsigned char local_clientid, int local_clienttype, const char * masterip, unsigned short masterport, const char * slaverip, unsigned short slaverport, const char * author_username, const char * author_pwd, const char * add_info);


/**
 * @brief      发送数据
 *
 * @param[in]  local_clientid  本地clientid
 * @param[in]  cmd             cmd：命令
 * @param[in]  cmdtype         命令类型，值为 2
 * @param[in]  dst_unitid      The destination unitid
 * @param[in]  dst_clientid    The destination clientid
 * @param[in]  dst_clienttype  The destination clienttype
 * @param[in]  data            The data
 * @param[in]  size            The size
 *
 * @return     0 表示成功、 < 0 表示错误。
 */
SMARTBUS_NET_CLI_API int CDECL SmartBusNetCli_SendData(unsigned char local_clientid, unsigned char cmd, unsigned char cmdtype, int dst_unitid, int dst_clientid, int dst_clienttype, const void * data, int size);


/**
 * @brief      SendPing
 *
 * @param[in]  local_clientid  本地clientid
 * @param[in]  dst_unitid      The destination unitid
 * @param[in]  dst_clientid    The destination clientid
 * @param[in]  dst_clienttype  The destination clienttype
 * @param[in]  data            The data
 * @param[in]  size            The size
 *
 * @return     0 表示成功、 < 0 表示错误。
 */
SMARTBUS_NET_CLI_API int CDECL SmartBusNetCli_SendPing(unsigned char local_clientid, int dst_unitid, int dst_clientid, int dst_clienttype, const void * data, int size);


/**
 * @brief      远程调用流程
 *
 * @param[in]  local_clientid  本地clientid
 * @param[in]  server_unitid   目标unitid
 * @param[in]  ipscindex       目标clientid
 * @param[in]  projectid       目标流程的ProjectID
 * @param[in]  flowid          目标流程的FlowID
 * @param[in]  mode            调用模式：0 有流程返回、1 无流程返回
 * @param[in]  timeout         有流程返回时的等待超时值。单位ms
 * @param[in]  in_valuelist    整型、浮点型、JSON数组（子流程开始节点的传人参数自动变换为dict类型数据。）（对应的字符串内容最大长度不超过16K字节）
 *
 * @return     > 0 invoke_id，调用ID，用于流程结果返回匹配用途。< 0 表示错误。
 */
SMARTBUS_NET_CLI_API int CDECL SmartBusNetCli_RemoteInvokeFlow(unsigned char local_clientid, int server_unitid, int ipscindex, const char * projectid, const char * flowid, int mode, int timeout, const char * in_valuelist);


/**
 * @brief      发送通知消息
 *
 * @param[in]  local_clientid  本地clientid
 * @param[in]  server_unitid   目标unitid ipscindex
 * @param[in]  processindex    目标clientid projectid
 * @param[in]  projectid       目标流程的ProjectID
 * @param[in]  title           通知的标示
 * @param[in]  mode            调用模式
 * @param[in]  expires         消息有效期。单位ms
 * @param[in]  param           消息数据
 *
 * @return     返回值：> 0 invoke_id，调用ID。< 0 表示错误。
 */
SMARTBUS_NET_CLI_API int CDECL SmartBusNetCli_SendNotify(unsigned char local_clientid, int server_unitid, int processindex, const char * projectid, const char * title, int mode, int expires, const char * param);

#ifdef __cplusplus
}
#endif



#endif
